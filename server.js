import express from "express";
import bodyParser from "body-parser";
import _ from "lodash";
import moment from "moment";
import { humanize } from "./services/moment";
import setupRoutes from "./routes";
import cors from "cors";

import { EXPIRATION_TIME as Test } from "./services/db";

export const init = (cb) => {
    return Promise.resolve().then(setupRoutes).then((routes) => {
        const app = express();

        if(process.env.NODE_ENV === "production") {
            app.enable("trust proxy");
        }
        
        app.use(bodyParser.json({ limit: "5mb" }));
        app.use(bodyParser.urlencoded({ limit: "5mb", extended: true }));

        app.use(cors({ origin: "*" }));
        app.locals.moment = moment;
        app.locals.humanize = humanize;
        app.locals.htmlEscape = html => _.escape(html).replace(/\n/g, "<br />");
        app.locals.calculateExpiration = post => {
            let {createdate, ttl} = post;
            let distance = ttl;
            let percent = ((Test - ttl) / Test) * 100;
            
            return { percent: 100 - Math.floor(percent), distance };
        }

        app.locals.calculateBrighterColor = (color) => {
            let hex = color.replace(/^\s*#|\s*$/g, "");
            let percent = 15;

            // convert 3 char codes --> 6, e.g. `E0F` --> `EE00FF`
            if(hex.length == 3){
                hex = hex.replace(/(.)/g, "$1$1");
            }
        
            var r = parseInt(hex.substr(0, 2), 16),
                g = parseInt(hex.substr(2, 2), 16),
                b = parseInt(hex.substr(4, 2), 16);
        
            return "#" +
               ((0|(1<<8) + r + (256 - r) * percent / 100).toString(16)).substr(1) +
               ((0|(1<<8) + g + (256 - g) * percent / 100).toString(16)).substr(1) +
               ((0|(1<<8) + b + (256 - b) * percent / 100).toString(16)).substr(1);
        }
    
        app.use(routes);
        app.use((req, res) => {
            res.status(404).json({
                title: "Not Found",
                error: "Sorry, the page you are looking for does not exist"
            });
        });
    
        app.listen(8081, () => cb(app));
    });
}