const Poly = require("poly");
const babelParser = require("poly/parsers/babel");

Poly.extend(require("poly/parsers/root-level-imports")(__dirname));
Poly.extend(babelParser({
    includes: [
        "memoryhole-common"
    ]
}));

require("./index");