import express from "express";
import db from "~/services/db";
import api from "./api";

const router = express.Router();

const injectJsonPayload = (req, res, next) => {
    res.payload = {
        _impl: {
            error: null,
            data: null
        },

        setError: ex => {
            let x = ex;
            while(x.message) {
                x = x.message;
            }

            res.payload._impl.error = x;
            return res;
        },

        setData: data => {
            if(!res.statusCode) {
                res.status(200);
            }

            res.payload._impl = { error: null, data };
            return res;
        }
    };


    res.sendPayload = () => res.json(res.payload._impl);
    return next();
}

router.use(injectJsonPayload);
router.use(api);

router.get("/health", (_, res) => res.status(200).send("OK"));

module.exports = async () => {
    await db.init();
    return router;
}