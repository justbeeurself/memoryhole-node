import express from "express";
import postsMiddleware from "./posts";

const router = express.Router();

router.use(postsMiddleware);

module.exports = router;