import express from "express";
import v1Api from "./v1";

const router = express.Router();

router.use("/v1", v1Api);
router.use(function(err, req, res, next) {
    console.error(err.stack)
    res.status(500).json({ error: err.message });
})

module.exports = router;