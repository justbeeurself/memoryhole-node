export const withSpec = (spec) => (req, res, next) =>
    Promise.resolve()
    .then(() => spec.validate(req.body))
    .then(() => next())
    .catch(ex => next(ex));