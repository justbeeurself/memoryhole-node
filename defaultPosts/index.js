import fs from "fs";
import path from "path";
import util from "util";

let readdir = util.promisify(fs.readdir);
let readFile = util.promisify(fs.readFile);

export default async () => {
    let rootDir = path.join(__dirname, "posts");
    let files = await readdir(rootDir);
    files = files.filter(x => x.substring(x.length - 5) === ".json");

    let pages = {};

    for(let file of files) {
        let _contents = await readFile(path.join(rootDir, file), { encoding: "utf-8" });
        const description = JSON.parse(_contents);
        const contents = await readFile(path.join(rootDir, description.file), { encoding: "utf-8" });

        description.details.content = contents;
        pages[description.details.uid] = description.details;
    }

    return pages;
}