import DatabaseDriver from "./common";
import createPostCache from "~/defaultPosts";
import nanoid from "nanoid";
import AWS from "aws-sdk";

const TableName = "memoryhole_posts";
const IdentityIndex = "identity-index";

export default class DynamoDBDriver extends DatabaseDriver {
    constructor(config) {
        super(config);

        const db = new AWS.DynamoDB.DocumentClient({
            accessKeyId: config.credentials[0],
            secretAccessKey: config.credentials[1],
            region: config.region || "us-east-1"
        });

        console.log("DynamoDB setup");
        this.db = db;
    }

    async init() {
        this.postCache = await createPostCache();
        console.log("Post cache setup");
    }
    
    getPostById = uid => new Promise((resolve, reject) => {
        if(uid in this.postCache) {
            return resolve(this.postCache[uid]);
        } else {
            let query = {
                TableName,
                KeyConditionExpression: "#uid = :uid",
                ExpressionAttributeNames: { "#uid": "uid" },
                ExpressionAttributeValues: { ":uid": uid },
                Limit: 1
            };
    
            this.db.query(query, (err, res) => {
                if(err) return reject(err);
                return resolve(res.Items[0]);
            });
        }
    });

    savePost = Item => new Promise((resolve, reject) => {
        let uid = nanoid(16);
        Item.uid = uid;

        Item.createDate = new Date().getTime();
        Item.expireDate = (
            Math.floor(Item.createDate / 1000) + this.config.expirationTime //TTL attribute managed via DynamoDB, in epoch time
        );
        
        this.db.put({
            TableName, Item
        }, (err, res) => {
            if(err) return reject(err);
            return resolve(uid);
        })
    });

    getPostsByIdentity = (identity = null, Limit = 100) => new Promise((resolve, reject) => {
        let cached = Object.values(this.postCache).filter(x => x.identity === identity);

        let query = {
            TableName,
            KeyConditionExpression: "#identity = :identity",
            IndexName: IdentityIndex,
            ExpressionAttributeNames: { "#identity": "identity" },
            ExpressionAttributeValues: { ":identity": identity },
            Limit
        };

        this.db.query(query, (err, data) => {
            if(err) return reject(err);
            return resolve(cached.concat(data.Items));
        });
    });
}