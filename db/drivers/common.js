export default class DatabaseDriver {
    constructor(config) {
        this.config = config;
        this.EXPIRATION_TIME = config.expirationTime;
    }

    async init() {
        return null;
    }

    async getPostById(id) {
        return {};
    }

    async savePost(post) {
        return null;
    }

    async getVanityIdentity(identity) {
        return null;
    }

    async saveVanityIdentity(identity, label) {
        return null;
    }

    async getPostsByIdentity(identity = null, Limit = 100) {
        return [];
    }
}