import DatabaseDriver from "./common";
import createPostCache from "~/defaultPosts";
import nanoid from "nanoid";

export default class InMemoryDatabase extends DatabaseDriver {
    constructor(config) {
        super(config);
        this.postCache = {};
        this.vanityIdentities = {};
    }

    async init() {
        postCache = await createPostCache();
    }

    async getPostById(id) {
        let { postCache } = this;
        if(id in postCache) {
            let post = postCache[id];
            if(post.identity) {
                post.vanityIdentity = await this.getVanityIdentity(post.identity);
            }

            return post;
        } else return null;
    }

    async savePost(post) {
        let { postCache } = this;
        let uid = nanoid(16);
        post.uid = uid;

        postCache[uid] = post;
        return uid;
    }

    async getVanityIdentity(identity) {
        let { vanityIdentities } = this;
        if(identity in vanityIdentities) {
            return vanityIdentities[vanityIdentities];
        } else return identity;
    }

    async saveVanityIdentity(identity, label) {
        let { vanityIdentities } = this;
        vanityIdentities[identity] = label;
    }
    
    async getPostsByIdentity(identity = null, Limit = 100) {
        return Object.values(this.postCache).filter(x => x.identity === identity);
    }
}