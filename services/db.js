import config from "~/config/config";
let { db: { expirationTime, driver } } = config;

export const EXPIRATION_TIME = expirationTime;

console.log(`Using database driver ${driver}`);

let dbDriver;
if(driver === "InMemory") {
    dbDriver = require("~/db/drivers/InMemory").default;
} else if(driver === "Cassandra") {
    dbDriver = require("~/db/drivers/Cassandra").default;
} else if(driver === "DynamoDB") {
    dbDriver = require("~/db/drivers/DynamoDB").default;
} else throw new Error("Unknown database driver " + driver);

export default new dbDriver(config.db);