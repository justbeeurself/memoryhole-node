import take from "lodash/take";

const pluralize = (word, count) => 
    count === 1 ? word : word + "s";

exports.humanize = (duration) => {
    const durationParts = [
        { value: duration.years(), unit: "year" },
        { value: duration.months(), unit: "month" },
        { value: duration.days(), unit: "day" },
        { value: duration.hours(), unit: "hour" },
        { value: duration.minutes(), unit: "minute" },
        { value: duration.seconds(), unit: "second" }
    ].filter((part) => part.value);

    // Display up to two of the most significant remaining parts
    return take(durationParts, 2).map((part) => (
        `${part.value} ${pluralize(part.unit, part.value)}`
    )).join(", ");
}