import { Recaptcha } from "express-recaptcha";
import config from "../config/config";
import util from "util";

const recaptcha = new Recaptcha(
    config.recaptcha.siteKey,
    config.recaptcha.siteSecret
);

const promisifiedVerify = util.promisify(recaptcha.verify).bind(recaptcha);
export default recaptcha;

export const middleware = async(req, res, next) => {
    try {
        let result = await promisifiedVerify({ body: { 'g-recaptcha-response': req.body.recaptcha } });
        return next();
    } catch(ex) {
        res.payload.setError(ex);
        return res.status(403).sendPayload();
    }
}