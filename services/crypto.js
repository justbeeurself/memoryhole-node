import crypto from "crypto";
import config from "~/config/config";

const salt = config.security.encryption.identitySalt;

exports.hash = (str = "") => {
    let hash = crypto.createHash("sha512");

    let half = str.substring(0, Math.floor(str.length / 2));
    let otherHalf = str.substring(Math.floor(str.length / 2));
    let salty = half + salt + otherHalf;

    hash.update(salty);
    return hash.digest("hex");
}

exports.md5 = (str = "") => {
    let hash = crypto.createHash("md5");

    let half = str.substring(0, Math.floor(str.length / 2));
    let otherHalf = str.substring(Math.floor(str.length / 2));
    let salty = half + salt + otherHalf;
    
    hash.update(salty);
    return hash.digest("hex");
}