export default {
    "recaptcha": {
        "siteKey": "",
        "siteSecret": ""
    },
    "security": {
        "encryption": {
            "identitySalt": "welcome to the memory hole, enjoy your stay"
        }
    },
    "db": {
        "driver": "InMemory",
        "expirationTime": 86400, //Time in seconds
        "credentials": []
    }
}