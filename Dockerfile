FROM node:10-alpine
ENV NODE_ENV=production
# Alpine doesn't come with git, so we need to install it
RUN apk add --update git && rm -rf /tmp/* /var/cache/apk/*
WORKDIR /home/node/app
COPY package*.json ./
RUN npm install
COPY . .
EXPOSE 8081
CMD [ "npm", "start" ]